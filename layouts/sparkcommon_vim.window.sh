# Set window root path. Default is `$session_root`.
# Must be called before `new_window`.
window_root "~/Projects/sparkcommon"

# Create new window. If no argument is given, window name will be based on
# layout file name.
new_window "sparkcommon_vim"

# Run commands.
run_cmd "vim"     # runs in active pane
