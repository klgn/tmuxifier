# Set window root path. Default is `$session_root`.
# Must be called before `new_window`.
window_root "~/src/python"

# Create new window. If no argument is given, window name will be based on
# layout file name.
new_window "python_vim"

# Run commands.
run_cmd "source ~/src/python/.3.10/bin/activate"
run_cmd "export PYTHONPATH=$HOME/src/python/.3.10/lib/python3.10/site-packages"
run_cmd "vim"
