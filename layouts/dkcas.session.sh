# Set a custom session root path. Default is `$HOME`.
# Must be called before `initialize_session`.
#session_root "~/Projects/dkcas"

# Create session with specified name if it does not already exist. If no
# argument is given, session name will be based on layout file name.
if initialize_session "dkcas"; then

  # Create a new window inline within session layout definition.
  new_window "dkcas"
  run_cmd "ssh cas4"
  run_cmd "cls"
  split_h 82
  run_cmd "ssh cas5"
  run_cmd "cls"
  split_h 75
  run_cmd "ssh cas6"
  run_cmd "cls"
  split_h 70
  run_cmd "ssh cas7"
  run_cmd "cls"
  split_h 50
  run_cmd "ssh cas8"
  run_cmd "cls"
  select_pane 1
  split_v 25
  run_cmd "ssh cas20"
  run_cmd "cls"
  select_pane 1
  split_v 30
  run_cmd "ssh cas14"
  run_cmd "cls"
  select_pane 1
  split_v 40
  run_cmd "ssh cas9"
  run_cmd "cls"
  select_pane 5
  split_v 25
  run_cmd "ssh cas23"
  run_cmd "cls"
  select_pane 5
  split_v 30
  run_cmd "ssh cas15"
  run_cmd "cls"
  select_pane 5
  split_v 40
  run_cmd "ssh cas10"
  run_cmd "cls"
  select_pane 9
  split_v 25
  run_cmd "ssh cas24"
  run_cmd "cls"
  select_pane 9
  split_v 30
  run_cmd "ssh cas17"
  run_cmd "cls"
  select_pane 9
  split_v 40
  run_cmd "ssh cas11"
  run_cmd "cls"
  select_pane 13
  split_v 25
  run_cmd "ssh cas25"
  run_cmd "cls"
  select_pane 13
  split_v 30
  run_cmd "ssh cas18"
  run_cmd "cls"
  select_pane 13
  split_v 40
  run_cmd "ssh cas12"
  run_cmd "cls"
  select_pane 17
  split_v 25
  run_cmd "ssh cas26"
  run_cmd "cls"
  select_pane 17
  split_v 30
  run_cmd "ssh cas19"
  run_cmd "cls"
  select_pane 17
  split_v 40
  run_cmd "ssh cas13"
  run_cmd "cls"
  select_pane 1

  # Load a defined window layout.
  #load_window "example"

  # Select the default active window on session creation.
  #select_window 1

fi

# Finalize session creation and switch/attach to it.
finalize_and_go_to_session
