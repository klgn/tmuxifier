# Set a custom session root path. Default is `$HOME`.
# Must be called before `initialize_session`.
#session_root "~/Projects/ukcas"

# Create session with specified name if it does not already exist. If no
# argument is given, session name will be based on layout file name.
if initialize_session "ukcas"; then

  # Create a new window inline within session layout definition.
  new_window "ukcas"
  run_cmd "ssh ukcas1"
  run_cmd "cls"
  split_h 82
  run_cmd "ssh ukcas2"
  run_cmd "cls"
  split_h 75
  run_cmd "ssh ukcas3"
  run_cmd "cls"
  split_h 70
  run_cmd "ssh ukcas4"
  run_cmd "cls"
  split_h 50
  run_cmd "ssh ukcas5"
  run_cmd "cls"
  select_pane 1
  split_v 25
  run_cmd "ssh ukcas16"
  run_cmd "cls"
  select_pane 1
  split_v 30
  run_cmd "ssh ukcas11"
  run_cmd "cls"
  select_pane 1
  split_v 40
  run_cmd "ssh ukcas6"
  run_cmd "cls"
  select_pane 5
  split_v 25
  run_cmd "ssh ukcas17"
  run_cmd "cls"
  select_pane 5
  split_v 30
  run_cmd "ssh ukcas12"
  run_cmd "cls"
  select_pane 5
  split_v 40
  run_cmd "ssh ukcas7"
  run_cmd "cls"
  select_pane 9
  split_v 25
  run_cmd "ssh ukcas18"
  run_cmd "cls"
  select_pane 9
  split_v 30
  run_cmd "ssh ukcas13"
  run_cmd "cls"
  select_pane 9
  split_v 40
  run_cmd "ssh ukcas8"
  run_cmd "cls"
  select_pane 13
  split_v 25
  run_cmd "ssh ukcas19"
  run_cmd "cls"
  select_pane 13
  split_v 30
  run_cmd "ssh ukcas14"
  run_cmd "cls"
  select_pane 13
  split_v 40
  run_cmd "ssh ukcas9"
  run_cmd "cls"
  select_pane 17
  split_v 25
  run_cmd "ssh ukcas20"
  run_cmd "cls"
  select_pane 17
  split_v 30
  run_cmd "ssh ukcas15"
  run_cmd "cls"
  select_pane 17
  split_v 40
  run_cmd "ssh ukcas10"
  run_cmd "cls"
  select_pane 1

  # Load a defined window layout.
  #load_window "example"

  # Select the default active window on session creation.
  #select_window 1

fi

# Finalize session creation and switch/attach to it.
finalize_and_go_to_session
