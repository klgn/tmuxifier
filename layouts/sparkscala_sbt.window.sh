# Set window root path. Default is `$session_root`.
# Must be called before `new_window`.
window_root "~/src/sparkscala"

# Create new window. If no argument is given, window name will be based on
# layout file name.
new_window "sparkscala_sbt"

# Run commands.
run_cmd "sbt"     # runs in active pane
