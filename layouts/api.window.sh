# Set window root path. Default is `$session_root`.
# Must be called before `new_window`.
window_root "~/src/api"

# Create new window. If no argument is given, window name will be based on
# layout file name.
new_window "api"

# Run commands.
run_cmd "source .3.10/bin/activate"    # runs in active pane
run_cmd "export PYTHONPATH=/home/klgn/src/api/.3.10/lib/python3.10/site-packages"     # runs in active pane

# Set active pane.
#select_pane 0
