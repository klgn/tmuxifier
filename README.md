# Tmuxifier
This repo contains my Tmuxifier layouts.

In order to use this repo, you need to clone the repo and then in your shell config file e.g. .zshrc or .bashrc you need to specify
`export TMUXIFIER_LAYOUT_PATH="/path/to/tmuxifier/layouts"` and then you should be good to go.
